<?php

namespace ATM\PointsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use \DateTime;

abstract class Point
{
    protected $id;

    /**
     * @ORM\Column(name="points", type="integer")
     */
    protected $points;

    /**
     * @ORM\Column(name="creationdate", type="datetime", nullable=false)
     */
    protected $creationdate;

    /**
     * @ORM\Column(name="is_paid", type="boolean", nullable=false)
     */
    protected $isPaid;

    public function __construct()
    {
        $this->isPaid = false;
        $this->creationdate = new DateTime();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getPoints()
    {
        return $this->points;
    }

    public function setPoints($points)
    {
        $this->points = $points;
    }

    public function getCreationdate()
    {
        return $this->creationdate;
    }

    public function setCreationdate($creationdate)
    {
        $this->creationdate = $creationdate;
    }

    public function getisPaid()
    {
        return $this->isPaid;
    }

    public function setIsPaid($isPaid)
    {
        $this->isPaid = $isPaid;
    }
}