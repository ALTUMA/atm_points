A Points Manager System

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require atm/pointsbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new ATM\PointsBundle\ATMPointsBundle(),
    ];
}
```

## Routing

Append to main routing file:

``` yml
# app/config/routing.yml
  
atm_points:
    resource: "@ATMPointsBundle/Resources/config/routing.yml"
    prefix:   /
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml
  
atm_points:
    class:
        model:
            point: Your Points class Namespace
            user: Your User class Namespace
            image_gallery: Your ImageGallery class Namespace 
            video: Your Video class Namespace 
        user_point_role: User role that is going to have points for example: 'ROLE_USER'
        gallery_points: Amount of points
        video_points: Amount of points
        description_points: Amount of points
        profile_image_points: Amount of points
        header_image_points: Amount of points
```


### Inheritance of classes ###

In order to use the ATMPointsBundle classes you will have to create your own Points class that will extend from the 
ATMPointsBundle's Points classes as shown bellow:

```php
namespace CoreBundle\Entity;
use ATM\PointsBundle\Entity\Point as BasePoint;

class Point extends BasePoint{

   public function __construct(){
        parent::__construct();
   }
}
```
### Twig Extension ###
You can use the following twig extension to know how many points a user has:

```twig
{{ getAtmUserPoints(user.id) }}
```