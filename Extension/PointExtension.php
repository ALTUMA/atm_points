<?php

namespace ATM\PointsBundle\Extension;

use ATM\PointsBundle\Services\PointsManager;

class PointExtension extends \Twig_Extension
{
    private $pointManager;


    public function __construct(PointsManager $pointManager){
        $this->pointManager = $pointManager;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('getAtmUserPoints', array($this, 'getAtmUserPoints')),
        );
    }

    public function getAtmUserPoints($userId){
        $totalPoints = $this->pointManager->getUserTotalPoints($userId);

        return $totalPoints;
    }


    public function getName()
    {
        return 'atm_points_bundle';
    }
}
