<?php

namespace ATM\PointsBundle\Services;

use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\Paginator;

class PointsManager{

    private $em;
    private $config;
    private $paginator;

    public function __construct(EntityManager $em,$config,Paginator $paginator)
    {
        $this->em = $em;
        $this->config = $config;
        $this->paginator = $paginator;
    }

    public function getUsersByRole($role){
        $qb = $this->em->createQueryBuilder();

        $andX = $qb->expr()->andX();

        $andX->add($qb->expr()->like('u.roles',$qb->expr()->literal('%'.$role.'%')));

        $qb
            ->select('u')
            ->from($this->config['class']['model']['user'],'u')
            ->where(
                $andX
            )
            ->orderBy('u.username');

        return $qb->getQuery()->getArrayResult();
    }

    public function getPoints($params){

        $default_params = array(
            'galleries' => false,
            'videos' => false,
            'fields' => false,
            'userId' => null,
            'page' => 1,
            'max_results' => 10
        );

        $params = array_merge($default_params,$params);

        $qbIds = $this->em->createQueryBuilder();
        $qbIds
            ->select('partial p.{id}')
            ->from($this->config['class']['model']['point'],'p')
            ->orderBy('p.creationdate','DESC');

        if($params['galleries']){
            $qbIds->join('p.imageGallery','ig');
        }

        if($params['videos']){
            $qbIds->join('p.video','v');
        }

        if($params['fields']){
            $qbIds
                ->leftJoin('p.imageGallery','ig')
                ->leftJoin('p.video','v')
                ->where(
                    $qbIds->expr()->andX(
                        $qbIds->expr()->isNull('ig.id'),
                        $qbIds->expr()->isNull('v.id')
                    )
                );
        }

        if(!empty($params['userId'])){
            $qbIds
                ->join('p.user','u','WITH',$qbIds->expr()->eq('u.id',$qbIds->expr()->literal($params['userId'])));
        }

        $ids = array_map(function($i){
            return $i['id'];
        },$qbIds->getQuery()->getArrayResult());


        if(count($ids) == 0){
            return array(
                'results' => array(),
                'pagination' => null
            );
        }

        $pagination = $this->paginator->paginate(
            $ids,
            $params['page'],
            $params['max_results']
        );

        $ids = $pagination->getItems();

        $qb = $this->em->createQueryBuilder();
        $qb
            ->select('p')
            ->addSelect('ig')
            ->addSelect('v')
            ->addSelect('u')
            ->from($this->config['class']['model']['point'],'p')
            ->join('p.user','u')
            ->leftJoin('p.imageGallery','ig')
            ->leftJoin('p.video','v')
            ->where(
                $qb->expr()->in('p.id',$ids)
            )
            ->orderBy('p.creationdate','DESC');

        $points = $qb->getQuery()->getArrayResult();

        return array(
            'results' => $points,
            'pagination' => $pagination
        );
    }

    public function getUserTotalPoints($userId){
        $qb = $this->em->createQueryBuilder();

        $qb
            ->select('SUM(p.points) as total')
            ->from($this->config['class']['model']['point'],'p')
            ->join('p.user','u','WITH',$qb->expr()->eq('u.id',$userId))
            ->where(
                $qb->expr()->eq('p.isPaid',0)
            );

        $result = $qb->getQuery()->getArrayResult();

        return $result[0]['total'];
    }

    public function checkUserFields($user){

        if(!$user->isDescriptionUpdated()){
            $description = $user->getDescription();
            if(!is_null($description)){
                $point = new $this->config['class']['model']['point'];
                $point->setUser($user);
                $point->setPoints($this->config['description_points']);
                $this->em->persist($point);
                $user->setDescriptionUpdated(true);
                $this->em->persist($user);
            }
        }

        if(!$user->isProfileImageUpdated()){
            $profileImage = $user->getProfileImage();
            if(!is_null($profileImage)){
                $point = new $this->config['class']['model']['point'];
                $point->setUser($user);
                $point->setPoints($this->config['profile_image_points']);
                $this->em->persist($point);
                $user->setProfileImageUpdated(true);
                $this->em->persist($user);
            }
        }

        if(!$user->isHeaderImageUpdated()){
            $headerImage = $user->getHeaderImage();
            if(!is_null($headerImage)){
                $point = new $this->config['class']['model']['point'];
                $point->setUser($user);
                $point->setPoints($this->config['header_image_points']);
                $this->em->persist($point);
                $user->setHeaderImageUpdated(true);
                $this->em->persist($user);
            }
        }

        $this->em->flush();
    }

    public function getPointsByParameter($key){
        if(isset($this->config[$key])){
            return $this->config[$key];
        }
         return false;
    }
}