<?php

namespace ATM\PointsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use \DateTime;

class PointsController extends Controller
{
    public function indexAction($userId)
    {
        $config = $this->getParameter('atm_points_config');
        $pointsManager = $this->get('atm_points_manager');
        $girls = $pointsManager->getUsersByRole($config['user_point_role']);
        return $this->render('ATMPointsBundle:Admin:index.html.twig',array(
            'users' => $girls,
            'userId' => $userId
        ));
    }

    public function galleriesTableAction($page,$userId){
        $pointsManager = $this->get('atm_points_manager');
        $params = array(
            'galleries' => true,
            'page' => $page,
            'userId' => $userId
        );
        $points = $pointsManager->getPoints($params);

        return $this->render('ATMPointsBundle:Admin:galleriesTable/table.html.twig',array(
            'points' => $points['results'],
            'pagination' => $points['pagination']
        ));
    }

    public function videosTableAction($page,$userId){
        $pointsManager = $this->get('atm_points_manager');
        $params = array(
            'videos' => true,
            'page' => $page,
            'userId' => $userId
        );
        $points = $pointsManager->getPoints($params);

        return $this->render('ATMPointsBundle:Admin:videosTable/table.html.twig',array(
            'points' => $points['results'],
            'pagination' => $points['pagination']
        ));
    }

    public function fieldsTableAction($page,$userId){
        $pointsManager = $this->get('atm_points_manager');
        $params = array(
            'fields' => true,
            'page' => $page,
            'userId' => $userId
        );
        $points = $pointsManager->getPoints($params);

        return $this->render('ATMPointsBundle:Admin:fieldsTable/table.html.twig',array(
            'points' => $points['results'],
            'pagination' => $points['pagination']
        ));
    }

    public function deletePointsAction($id){
        $configuration = $this->getParameter('atm_points_config');
        $em = $this->getDoctrine()->getManager();

        $point = $em->getRepository($configuration['class']['model']['point'])->findOneById($id);
        $em->remove($point);
        $em->flush();

        return new Response('ok');
    }

    public function getUserMediaAction($userId){
        $em = $this->getDoctrine()->getManager();
        $currentDate = new DateTime('+1 day');
        $configuration = $this->getParameter('atm_points_config');

        $qbVideos = $em->createQueryBuilder();
        $qbVideos
            ->select('v')
            ->from($configuration['class']['model']['video'],'v')
            ->join('v.user','u','WITH',$qbVideos->expr()->eq('u.id',$userId))
            ->leftJoin('v.points','p')
            ->where(
                $qbVideos->expr()->andX(
                    $qbVideos->expr()->lte('v.publishdate',$qbVideos->expr()->literal($currentDate->format('Y-m-d'))),
                    $qbVideos->expr()->isNull('p.id')
                )
            )
            ->orderBy('v.title','ASC');
        $resVideos = $qbVideos->getQuery()->getArrayResult();


        $qbGalleries = $em->createQueryBuilder();
        $qbGalleries
            ->select('partial ig.{id,title}')
            ->from($configuration['class']['model']['image_gallery'],'ig')
            ->join('ig.user','u','WITH',$qbGalleries->expr()->eq('u.id',$userId))
            ->leftJoin('ig.points','p')
            ->where(
                $qbGalleries->expr()->andX(
                    $qbGalleries->expr()->lte('ig.publishdate',$qbGalleries->expr()->literal($currentDate->format('Y-m-d'))),
                    $qbGalleries->expr()->isNull('p.id')
                )
            )
            ->orderBy('ig.title','ASC');
        $resGalleries = $qbGalleries->getQuery()->getArrayResult();

        $htmlVideos = '';
        foreach($resVideos as $video){
            $htmlVideos .= '<option value="'.$video['id'].'">'.$video['title'].'</option>';
        }

        $htmlGalleries = '';
        foreach($resGalleries as $gallery){
            $htmlGalleries .= '<option value="'.$gallery['id'].'">'.$gallery['title'].'</option>';
        }

        return new Response(json_encode(
            array(
                'videos' => $htmlVideos,
                'galleries' => $htmlGalleries
            )
        ));
    }

    public function submitAddPointsAction(){
        $configuration = $this->getParameter('atm_points_config');
        $request = $this->get('request_stack')->getCurrentRequest();
        $em = $this->getDoctrine()->getManager();
        $point = new $configuration['class']['model']['point']();

        $userId = $request->get('girlId');
        $user = $em->getRepository($configuration['class']['model']['user'])->findOneById($userId);
        $point->setUser($user);

        $galleryId = $request->get('galleryId');
        $videoId = $request->get('videoId');
        if($galleryId) {
            $imageGallery = $em->getRepository($configuration['class']['model']['image_gallery'])->findOneById($galleryId);
            $point->setImageGallery($imageGallery);
        }else{
            $video = $em->getRepository($configuration['class']['model']['video'])->findOneById($videoId);
            $point->setVideo($video);
        }

        $points = $request->get('points');
        $point->setPoints($points);

        $date = $request->get('date');
        $dateTime = DateTime::createFromFormat('d-m-Y', $date);
        $point->getCreationdate($dateTime);

        $em->persist($point);
        $em->flush();

        return new RedirectResponse($this->get('router')->generate('atm_points_admin_index'));
    }
}
