<?php

namespace ATM\PointsBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $treeBuilder->root('atm_points')
                        ->children()
                            ->arrayNode('class')->isRequired()
                                ->children()
                                    ->arrayNode('model')->isRequired()
                                        ->children()
                                            ->scalarNode('point')->isRequired()->end()
                                            ->scalarNode('user')->isRequired()->end()
                                            ->scalarNode('image_gallery')->isRequired()->end()
                                            ->scalarNode('video')->isRequired()->end()
                                        ->end()
                                    ->end()
                                ->end()
                            ->end()
                            ->scalarNode('user_point_role')->isRequired()->end()
                            ->scalarNode('gallery_points')->isRequired()->end()
                            ->scalarNode('video_points')->isRequired()->end()
                            ->scalarNode('description_points')->isRequired()->end()
                            ->scalarNode('profile_image_points')->isRequired()->end()
                            ->scalarNode('header_image_points')->isRequired()->end()
                        ->end()
                    ->end();

        return $treeBuilder;
    }
}
