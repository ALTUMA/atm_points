// Unlock new content v1.0 - jQuery Box plugin
// (c) 2015 Xavi Mateos - http://es.linkedin.com/pub/xavi-mateos/12/8ab/529
// License: http://www.opensource.org/licenses/mit-license.php
(function( $ ) {
    
    $.fn.box = function(method) {
        
        var settings;
        var overlay_bg;
        var content;
        var secret; // for caching the box content in all links sharing box (based on settings.boxContentURL)

        var generateId = function makeid(length) {
            var text = "";
            var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            for( var i=0; i < length; i++ )
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            return text;
        }
        
        var createBoxContent = function() {
            if($('#stiffiaBoxContent[data-secret="' + secret + '"]').length > 0)
            {
                box_content = $('body').find('#stiffiaBoxContent[data-secret="' + secret + '"]');
            } else {
                box_content = $('<div id="stiffiaBoxContent" data-secret="' + secret + '" />');
                box_content.css({
                    'margin': '0 auto',
                    'position': 'fixed',
                    'left': '0',
                    'top': '0',
                    'bottom': '0',
                    'right': '0', 
                    'background': 'transparent',
                    'overflow': 'auto',
                    'z-index': '2147483646',
                    'display': 'none'
                });
                if(settings.height != '')
                {
                    box_content.css({
                        'min-height': (settings.height - 1) + 'px'
                    });
                }
            }
            return box_content;
        };

        var methods = {
            init : function(options) { // Fired when the plugin is initialized (once)

                this.box.settings = $.extend({}, this.box.defaults, options);
                settings = this.box.settings;

                $(document).ready(function(){
                    settings.pluginOnLoad(); // Custom function on popup show
                });

                return this.each(function(i) {
                    //console.log('once');
                    var link = $(this);
                    //secret = btoa(link.attr('id'));
                    if(settings.container != '')
                    {
                        secret = btoa(settings.container);
                    } else {
                        secret = btoa(settings.route);
                    }
                    //secret = generateId(10);

                    methods.createBackground();
                    content = createBoxContent();

                    // create main close button
                    if($("#jqBoxMainClose").length <= 0)
                    {
                        var mainCloseButton;
                        if(settings.mainCloseButton == '')
                        {
                            mainCloseButton = $('<a />');
                            mainCloseButton
                                .addClass('close')
                                .attr('alt','close this')
                                .attr('href','javascript:;')
                                .attr('id','jqBoxMainClose')
                                .css({
                                    'z-index':'2147483647',
                                    'color':'#FFF',
                                    'font-size': '40px',
                                    'position': 'fixed',
                                    'right': '20px',
                                    'top': '11px'
                                })
                                .html('<i class="fa fa-times"></i>');
                        } else {
                            mainCloseButton = settings.mainCloseButton;
                            mainCloseButton.attr('id','jqBoxMainClose');
                        }
                        mainCloseButton.bind('click.jqBox', function(e){
                            methods.closeBox(e);
                        });
                        content.append(mainCloseButton);

                        // Selectors set for closing the box
                        if(settings.closeBox.length > 0)
                        {
                            $.each(settings.closeBox, function(i,val){
                                content.find(val).bind('click.jqBox', function(e){
                                    methods.closeBox(e);
                                })
                            });
                        }
                    }
                    $('body').append(content);

                    content.bind('click.jqBox', function(e){
                        methods.closeBox(e);
                    });

                    link.bind('click.box', function(e){
                        methods.showBox(e, content);
                    });
                });
            },
            createBackground : function(e) {
                // Check box object exists; if not, create it once
                if($("#stiffiaBox").length > 0)
                {
                    overlay_bg = $('body').find('#stiffiaBox');
                } else {
                    // First time - create layers
                    overlay_bg = $('<div id="stiffiaBox" />');

                    overlay_bg.css({
                        'background': 'url(/bundles/frontend/common/images/thumb_bg.png) repeat',
                        'width': '100%',
                        'height': '100%',
                        'display': 'block',
                        'position': 'fixed',
                        'left': '0',
                        'top': '0',
                        'bottom': '0',
                        'right': '0',
                        'text-align': 'center',
                        'z-index': '2147483645',
                        'position': 'fixed',
                        'display': 'none'
                    });

                    overlay_bg.bind('click.jqBox', function(e){
                        methods.closeBox(e);
                    }).children().bind('click.jqBox', function(e){
                        methods.preventClose(e);
                    });

                    // create box
                    $('body').append(overlay_bg);
                }
            },
            showBox: function(e, content) {
                if(typeof e != 'undefined')
                {
                    e.preventDefault();
                }
                // first check if the backButton plugin is in use, so we set it to not to track ajax calls made within the plugin
                if(typeof ajaxHistory != 'undefined')
                {
                    ajaxHistory.setValue("noTrack",true);
                }
                
                //methods.createBackground();

                if(settings.container != '')
                {
                    //var cloned_container = $(settings.container).clone(true);
                    //$(settings.container).remove(); // remove original
                    var cloned_container = $(settings.container);
                    //console.log($('.btnRegisterLogin').length);
                    cloned_container.css({
                        'display': 'block'
                    });
                    //content.html('');
                    content.append(cloned_container);
                    //console.log($('.btnRegisterLogin').length);
                    overlay_bg.fadeIn('slow');
                    $('body').css({
                        'overflow': 'hidden'
                    });
                    
                    // Show box content
                    //overlay_bg.append(content);
                    //content.bind('click.box', methods.preventClose)
                    
                    $(document).bind('keydown.jqBox', function(e) { 
                        if(e.which == 27) {
                            if($("#stiffiaBox").length > 0)
                            {
                                methods.closeBox(e);
                            }
                        }
                    });
                    
                    content.show();
                    
                    // Adjust parent size
                    $("#stiffiaBoxContent").animate({
                        'width': '100%',
                        'height': '100%',
                        'margin': '0',
                        'top':'0',
                        'left':'0',
                        'right':'0',
                        'bottom':'0',
                        'z-index': '2147483646',
                        'overflow': 'auto',
                        'background': 'transparent'
                    },600);
                } else {
                    // Load from route
                    if(settings.route != '')
                    {
                        // Load content within the box
                        content.load(settings.route, function(){
                           overlay_bg.fadeIn('slow');
                            $('body').css({
                                'overflow': 'hidden'
                            });
                            
                            // Show box content
                            $('body').append(content);
                            
                            content.show();
                        });
                    } else {
                        // Nothing to load
                        console.log('Nothing to load');
                    }
                }
            },
            preventClose : function(e) {
                //console.log('Prevented!');
                e.stopPropagation();
            },
            closeBox: function(e) {
                //if(e !== undefined)
                //{ 
                    var targetId = e.target.id;
                    var closeElements = settings.closeBox.join();
                    if(closeElements.indexOf(targetId) != -1 && targetId != '')
                    {
                        settings.pluginBeforeUnload(); // Custom function before closing popup
                        overlay_bg.hide();
                        content.hide();
                        $('#stiffiaBoxContent').hide();
                        if(settings.container != '')
                        {
                            $('body').append($(settings.container)); // restore original
                            $(settings.container).css({
                                'display': 'none'
                            });
                            //content.find(settings.container).remove();
                            settings.pluginOnUnload(); // Custom function on popup closed
                        }
                        //overlay_bg.remove();
                        $('body').css({
                            'overflow': ''
                        });
                        $(document).unbind('keydown.jqBox');
                    }
                //}
                return false;
            },
        };
        
        // Method calling logic
        if ( methods[method] )
        {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else {
            if ( typeof method === 'object' || ! method )
            {
                return methods.init.apply( this, arguments );
            } else {
                $.error( 'Method ' +  method + ' does not exist on jQuery.box' );
            }
        }
    };

    $.fn.box.settings = {};
    
    // Without selector, for generic usage
    $.box = function(options) {
        var phantomLink = $('<a />');
        phantomLink.box(options);
        phantomLink.click();
        return this;
    };
    
    // plugin's default options
    $.fn.box.defaults = {
        'container'         : '',// selector of the containder holding the box content (must exist in DOM),
        'route'             : '', // action that will render its output within the box
        'pluginOnLoad'      : function pluginOnLoad(){}, // custom function loaded on showPopup event
        'pluginBeforeUnload': function pluginBeforeUnload(){}, // custom function loaded on popup close
        'pluginOnUnload'    : function pluginOnUnload(){}, // custom function loaded on popup close
        'closeBox'          : [], // array of selectors that will close the popup
        'mainCloseButton'   : '', // main big button, placed on top right, with class 'jqboxMainClose' (if not set differently)
    }
    
})( jQuery );