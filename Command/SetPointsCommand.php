<?php

namespace ATM\PointsBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use \DateTime;

class SetPointsCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('set:atm:points');
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $config = $this->getContainer()->getParameter('atm_points_config');
        $pointsManager = $this->getContainer()->get('atm_points_manager');

        $date = new DateTime();
        $currentDate = $date->format('Y-m-d');
        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');
        $qbImageGalleries = $em->createQueryBuilder();

        $qbImageGalleries
            ->select('ig')
            ->addSelect('p')
            ->addSelect('u')
            ->from($config['class']['model']['image_gallery'],'ig')
            ->leftJoin('ig.points','p')
            ->join('ig.user','u')
            ->where(
                $qbImageGalleries->expr()->andX(
                    $qbImageGalleries->expr()->gte('ig.publishdate',$qbImageGalleries->expr()->literal($currentDate.' 00:00:00')),
                    $qbImageGalleries->expr()->lte('ig.publishdate',$qbImageGalleries->expr()->literal($currentDate.' 23:59:59'))
                )
            );

        $imageGalleries = $qbImageGalleries->getQuery()->getResult();

        $progress = $this->getHelper('progress');
        $progress->start($output, count($imageGalleries));

        $count = 0;
        foreach($imageGalleries as $imageGallery){
            if(is_null($imageGallery->getPoints())){
                $user = $imageGallery->getUser();

                $points = $pointsManager->getUserTotalPoints($user->getId());

                if($points < 100){
                    $publishdate = $imageGallery->getPublishdate();
                    $points = new $config['class']['model']['point']();
                    $points->setUser($user);
                    $points->setImageGallery($imageGallery);
                    $points->setPoints($config['gallery_points']);
                    $points->setCreationdate($publishdate);
                    $em->persist($points);
                    $em->flush();
                }

            }

            $count++;
            $progress->setCurrent($count);
        }

        $progress->finish();

        $qbVideos = $em->createQueryBuilder();
        $qbVideos
            ->select('v')
            ->addSelect('p')
            ->addSelect('u')
            ->from($config['class']['model']['video'],'v')
            ->leftJoin('v.points','p')
            ->join('v.user','u')
            ->where(
                $qbVideos->expr()->andX(
                    $qbVideos->expr()->gte('v.publishdate',$qbVideos->expr()->literal($currentDate.' 00:00:00')),
                    $qbVideos->expr()->lte('v.publishdate',$qbVideos->expr()->literal($currentDate.' 23:59:59'))
                )
            );

        $videos = $qbVideos->getQuery()->getResult();
        $progress1 = $this->getHelper('progress');
        $progress1->start($output, count($videos));

        $count = 0;
        foreach($videos as $video){
            if(is_null($video->getPoints())){
                $user = $video->getUser();
                $points = $pointsManager->getUserTotalPoints($user->getId());

                if($points < 100){
                    $publishdate = $video->getPublishdate();
                    $points = new $config['class']['model']['point']();
                    $points->setUser($user);
                    $points->setVideo($video);
                    $points->setPoints($config['video_points']);
                    $points->setCreationdate($publishdate);
                    $em->persist($points);
                    $em->flush();
                }
            }

            $count++;
            $progress1->setCurrent($count);
        }
        $progress1->finish();
    }
}
